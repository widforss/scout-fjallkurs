# Fjällkurs

## Ledarskap och kunskapsvägledning

### Målsättning

* Tillämpa ett situationsanpassat ledarskap som främjar en trivsam stämning under aktiviteten 
* Förmåga att bedöma gruppens möjligheter att klara av aktuell aktivitet utifrån rådande förhållanden – ”Tur efter förmåga” 
* Förmåga att hantera nödsituationer

### Innehåll

* Ledarkompetens
* Pedagogik i fjällmiljö

## Fysisk prestationsförmåga

### Målsättning

* Förståelse för kroppens förutsättningar att fungera under skiftande yttre och inre förhållanden 

### Innehåll

* Kost och vätska
* Miljö och anpassning av klädsel

## Förstahjälpen

### Målsättning

* Kunna göra en lägesbedömning samt ge första hjälpen vid olycksfall
* Förstå hur kyla påverkar kroppen samt kunna vidta korrekta åtgärder 

### Innehåll

* Allmän nedkylning
* Lokal kylskada (ytlig eller djup)
* KFI-skada (Kyla, Fukt, Immobilitet) 

## Orientering och förflyttning

### Målsättning

* Kunna leda förflyttning effektivt och under säkra former till ett bestämt mål i okänd terräng 

### Innehåll

* Karta och kompass (bl.a. karttecken, skalor, orientering dager och mörker) 
* Turplanering (bl.a. tid, svårighet, vägval, utrustning)

## Utrustning

### Målsättning

* Förståelse för fjällutrustningens möjligheter och begränsningar i förhållande till aktuell aktivitet och omständigheter 
* Förståelse för utrustningens behov av vård och underhåll

### Innehåll

* Olika materials egenskaper (isolerande, vind- och vattentätt, ventilerande etc.) 
* Lämplig klädsel beroende på aktivitet, väder och klimat
* Handhavande av friluftsutrustning (ex. olika typer av; tält, vindsäckar, ryggsäckar, sovsäckar, stormkök, kängor, skidor m.m.) 

## Väder

### Målsättning

* Kunna inhämta väderprognoser och omsätta informationen till vad det faktiskt kan innebära för genomförandet av tilltänkt aktivitet

### Innehåll

* Vilka källor som finns och hur de kontaktas
* Tolkning av aktuella fjällväderprognoser

## Snö- och lavinkunskap

### Målsättning

* Förståelse för fenomenet lavin
* Söka och tolka information om lavinrisk
* Kunna identifiera och undvika lavinfarlig terräng
* Kunna genomföra kamraträddning

### Innehåll

* Genomförande av förstahjälpen vid lavin

## Iskunskap

### Målsättning

* Kunna välja säker väg över isbelagda vattendrag 

### Innehåll

* Iskunskap
* Vägvalsbedömningar under övergång

## Bivack och nödbivack

### Målsättning

* Kunna etablera tillfälligt skydd i vintermiljö i syfte att ge vila, värme och återhämtning 

### Innehåll

* Upprätta nattläger med hjälp av tält
* Risker med grävarbete och boende i snö
* Olika typer av snöbivacker för kortare eller längre vistelse 
* Övernattning i snöbivack

## Fjällräddning

### Målsättning

* Kunskap om svensk fjällräddnings organisation och dess resurser 

### Innehåll

* Larmning och samverkan med fjällräddning
* Presentation av hur fjällräddningen är organiserad

## Helikoptersäkerhet

### Målsättning

* Kännedom om korrekt samverkansprocedur med helikopter

### Innehåll

* Uppträdande omkring helikopter
* Tecken, signaler och förberedelse av landningsplats 

## Naturvård

### Målsättning

* Kännedom om möjligheter och begränsningar för organiserat friluftsliv i fjällnatur 

### Innehåll

* Allemansrätt
* Skoter
* Stugvett

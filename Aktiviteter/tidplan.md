# Tidplan

## Fredag

Kursförberedelser

## Lördag

* Förflyttning av människor och packning
* Inflyttning
* Praktisk genomgång och kursintro
* Brandinfo

## Söndag

* Förmiddag
    * Väder och utrustning (Vintermiljö i boken)
    * Fysisk prestationsförmåga
* Eftermiddag
    * Orienteringövning, lek [FJÄLL]
        * Plocka utrustning och använd den under vägen
        * Gör kort färdmeddelande till de två nästkommande stationerna
        * Sätta upp tält
* Kväll
    * Andas
    * Dela ut bok

## Måndag

* Heldag på fjället
    * Förflyttning
    * Snö
    * Lavin
        * Var går laviner
        * Varför går laviner
    * Is
    * Fjällaktivitet
* Kväll
    * Packa rätt för övernattning

## Tisdag - onsdag

* Dela in i patruller
* Tälttur
    * Utbildning
        * Första hjälpen
        * Kamraträddning
        * Helikoptersamverkan
    * Fjällaktivitet
    * Testa snöbivack
    * Testa nödbivack

## Torsdag

* Övning
    * Fjällräddning
    * Fjällaktivitet
* Nödbivack
* Nattorientering
* Spökprogram
    * Lavinövning
    * Naturvård
    * Ledarskap
    * Vård

## Fredag
* Förmiddag
    * Sova
* Eftermiddag
    * Materialvård
* Kväll
    * Ceremoni

## Lördag

* Städning
* Förflyttning av människor och packning

# Uppföljning

* i slutet av maj
* september-oktober
* januari
* tre veckor innan

# Tidplan år 2

## Fredag

* Förberedelse för kurshandledarna

## Lördag


## Söndag

* Förberedelsedag för ledarna
* Barnen kommer söndag kväll
* Barnpresentation med brand osv.

## Måndag

* Kursdag
* De har gården för sig själva

## Tisdag

* Kursdag

## Onsdag

* Kursdag

## Torsdag

* Kursdag

## Fredag

* Barnen åker hem.
* Utvärdering.

## Lördag


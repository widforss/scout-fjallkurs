# Aktivitetsförslag

## Bygga

* Snöka
* Snöskulptur
* Snöfort

## Göra

* Vinter-OS
    * Nöjdhopp
    * Snöbollsstötning
    * Skidstavekastning
    * Gropgrävning
    * Lavinsondsbingo
* Orientering
* Topptur
* Tälttur
* Stugtur
* Finmiddag i snön
* Hinderbana på skidor
* Fototävling

# Moment

* Navigering
* Färdplanering
* Använda säkerhetsutrustning
* Ta rast
* Kyla och vind
* Dagsutrustning
* Leda grupp på fjäll
* Förberedd matlagning
* Tredagars tur
    * En natt nödbivack
* Utrustning
    * Innan tur
    * Under tur
* Laviner
    * Undvika laviner
    * Kamraträddning
    * Fjällräddning
* Fjällräddning

## Utifrån kursmål

### Ledarskap och kunskapsvägledning

### Fysisk prestationsförmåga

### Första hjälpen

### Orientering och förflyttning

### Utrustning

### Väder

* Patrullen inhämtar väderprognos varje morgon.

### Snö- och lavinkunskap

### Iskunskap

### Bivack och nödbivack

### Fjällräddning

### Helikoptersäkerhet

### Naturvård


